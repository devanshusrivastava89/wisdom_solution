# Wisdom_Solutions
# Used Laravel Version : 8.x
# Stallation Steps
# 1. Take git pull
# 2. Change .env according to local setup
# 3. Run ===> composer update
# 4. Run ===> php artisan migrate
# 5. Run ===> npm install (if required)
# 6. Run ===> npm run dev (if required)
# 7. Run ===> php artisan serve
# If Required please run ===> php artisan key:generate
# and again start project using ===> php artisan serve