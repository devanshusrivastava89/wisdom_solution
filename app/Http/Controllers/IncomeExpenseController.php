<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\IncomeExpense;

class IncomeExpenseController extends Controller
{
	public function index()
    {
    	$income = IncomeExpense::where('type', 'income')->sum('value');
    	$expense = IncomeExpense::where('type', 'expense')->sum('value');
    	$total = $income - $expense;
    	$all = IncomeExpense::orderBy('id', 'desc')->get();

        return view('home',['total'=>$total,'all'=>$all]);
    } 

    public function save(Request $req){

    		$req->validate([
			    'type' => 'required',
			    'amount' => 'required',
			    'comment' => 'required',
			]);

			$obj = new IncomeExpense();
			$obj->type = $req->input('type');
			$obj->value = $req->input('amount');
			$obj->details = $req->input('comment');
    		$obj->save();
    		//return route('home');
    		return redirect()->route('home')->with('success','Success');
    }
}
