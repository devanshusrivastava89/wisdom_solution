@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Total balance') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ $total }}
                </div>
                
            </div>
            <div class="card">
                <div class="card-header">{{ __('Income/Expense Entry Form') }}</div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                </div>
                 @endif
                <div class="card-body">
                <form method="POST" action="/save">
                        @csrf
                    <label for="type">Type</label>
                    <select id="type" name="type" required>
                        <option value="income">Income</option>
                        <option value="expense">Expense</option>

                    </select><br /><br />
                    <label for="amount">Amount</label>
                    <input id="amount" name="amount" type="number" min="1" class="@error('title') is-invalid @enderror" required><br /><br />
                    <label for="comment">Comment</label>
                    <input id="comment" name="comment" type="text" class="@error('title') is-invalid @enderror" required><br/><br />
                    <button type="Submit">Submit</button>
       
                    </form>
                </div>
            </div>

<style type="text/css">
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}

th {
  text-align: left;
}
</style>
            <div class="card">
                <div class="card-header">{{ __('Report') }}</div>
                <div class="card-body">
                     <table style="width:100%">
                      <tr>
                        <th>Id</th>
                        <th>Type</th>
                        <th>Amount</th>
                        <th>Details</th>
                        <th>Created At</th>
                      </tr>
                      @foreach($all as $key)
                      <tr>
                        <td>{{ $key->id }}</td>
                        <td>{{ $key->type }}</td>
                        <td>{{ $key->value }}</td>
                        <td>{{ $key->details }}</td>
                        <td>{{ $key->created_at }}</td>
                      </tr>
                      @endforeach
                    </table> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
